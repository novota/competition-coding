# Dynamical programming challenges
# Link to problem
# https://www.spoj.com/problems/NY10E/

import math

def main():

	nr_of_inputs = int(input())
	inputs = []
	while nr_of_inputs > 0:
		order, nr_digits = map(int, input().split())
		inputs.append((order, nr_digits))
		nr_of_inputs -= 1

	base = 9
	for order, nr_digits in inputs:
		result = int(math.factorial(base + nr_digits) / (math.factorial(base) * math.factorial(nr_digits)))
		print(str(order) + " " + str(result))

if __name__ == "__main__":
	main()