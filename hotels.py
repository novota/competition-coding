# Dynamical programming
# Link to problem
# https://www.spoj.com/problems/HOTELS/


def get_result(values: list, money_total:int):

	idx_start, idx_end = 0, 1
	curr_sum, best_sum = values[idx_start], 0

	if (curr_sum > best_sum) and (curr_sum <= money_total):
		best_sum = curr_sum

	while idx_end < len(values):
		curr_sum += values[idx_end]

		while curr_sum > money_total:
			curr_sum -= values[idx_start]
			idx_start += 1

		if curr_sum >= best_sum:
			best_sum = curr_sum

		if best_sum == money_total:
			return best_sum

		idx_end += 1

	return best_sum



def main():

	nr_values, money_total = map(int, input().split())
	values = list(map(int, input().split()))

	result = get_result(values, money_total)
	print(result)


if __name__ == "__main__":
	main()