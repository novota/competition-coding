# DP problems
# Link to problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=3&page=show_problem&problem=83
memory = {}

def count_ways(money_left: int, money, previous_coin = 10000):
	if money_left == 0:
		return 1

	if (previous_coin, money_left) in memory:
		return memory[(previous_coin, money_left)]

	result = 0
	for coin_value in money:
		if (coin_value <= money_left) and (coin_value <= previous_coin):
			result += count_ways(money_left - coin_value, money, coin_value)

	memory[(previous_coin, money_left)] = result
	return result


def create_out(case: int, nr_spaces: int):
	case = float(case / 100)
	case = f"{case:.2f}"

	res = ""
	while len(case) < nr_spaces:
		res += " "
		nr_spaces -= 1
	return res + case

def create_out_int(case: int, nr_spaces: int):
	case = str(case)

	res = ""
	while len(case) < nr_spaces:
		res += " "
		nr_spaces -= 1
	return res + case


def main():
	money = [10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5]
	new_nbr = int(float(input()) * 100 + 0.5)
	inputs = []

	while new_nbr != 0:
		inputs.append(new_nbr)
		new_nbr = int(float(input()) * 100 + 0.5)

	for case in inputs:
		res = count_ways(case, money)

		print(f"{create_out(case,6)}{create_out_int(res,17)}")



if __name__ == "__main__":
	main()