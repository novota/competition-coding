# DP challenges
# Link to problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=655&page=show_problem&problem=2402

memory = {}
def find_smallest_n(value: int, squares: list):

	if value == 0:
		return 0

	if value in memory:
		return memory[value]

	
	best_result = 100000
	for square in squares:
		result = 0
		if square <= value:
			result += find_smallest_n(value - square, squares)
			if result < best_result:
				best_result = result
	memory[value] = best_result + 1
	return best_result + 1

def main():
	squares = []
	for i in range(100, 0, -1):
		squares.append(i*i)

	inputs = []
	nr_inputs = int(input())
	while nr_inputs > 0:
		inputs.append(int(input()))
		nr_inputs -= 1

	for value in inputs:
		print(find_smallest_n(value, squares))


if __name__ == "__main__":
	main()