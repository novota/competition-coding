# Dynamical programming
# Link to problem
# https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=8&category=532&page=show_problem&problem=3794
import math

def decode_input(input_tuple:str):
	return int(input_tuple[: input_tuple.find(" ")]), int(input_tuple[input_tuple.find(" ") + 1: ])


def creates_triangle(x):
	lower_bound = x // 3
	for triangle_length in range(lower_bound, x):
		if (triangle_length * (triangle_length + 1)) / 2 == x:
			return True
		if (triangle_length * (triangle_length + 1)) / 2 > x:
			return False

def alternative_solver(a, b):
	result = 0
	square = math.sqrt(a + 1)
	if not square.is_integer():
		square = int(square) + 1

	while square**2 < b:
		if creates_triangle(square**2 - 1):
			result += 1

		square += 1
	return result

def main():
	input_cases = []
	input_tuple = input("input")
	a, b = decode_input(input_tuple)

	while a != 0 and b != 0:
		input_cases.append((a, b))
		# next input
		input_tuple = input()
		a, b = decode_input(input_tuple)

	case_nr = 1
	for a, b in input_cases:
		result = alternative_solver(a, b)
		print(f"Case {case_nr}: {result}")



if __name__ == "__main__":
	main()