# DP programming
# Link to problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=653&page=show_problem&problem=2078

memory = {}


def get_possibilities(amount: int, coins, previous_coin=9261):

	if amount == 0:
		return 1

	if (previous_coin, amount) in memory:
		return memory[(previous_coin, amount)]

	result = 0
	for coin in coins:
		if (coin <= amount) and (coin <= previous_coin):
			result += get_possibilities(amount - coin, coins, coin)

	memory[(previous_coin, amount)] = result
	return result


def main():
	coins = []
	for i in range(21, 0, -1):
		coins.append(i*i*i)
	inputs = []
	while True:
		try:
			inputs.append(int(input()))
		except:
			break

	for amount in inputs:
		print(get_possibilities(amount, coins))


if __name__ == "__main__":
	main()