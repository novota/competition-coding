# DP programming
# Link to problem
# https://www.spoj.com/problems/GERGOVIA/


def shortest_path(case: list):

	curr_count = 0
	result = 0
	for _, number in enumerate(case):
		result += abs(curr_count)
		curr_count += number

	return result


def main():

	inputs = []
	nr_houses = int(input())

	while nr_houses > 0:
		inputs.append(list(map(int, input().split())))
		nr_houses = int(input())

	for case in inputs:
		print(shortest_path(case))


if __name__ == "__main__":
	main()