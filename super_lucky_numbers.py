# Programming challenges
# Link to problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&category=0&problem=1663&mosmsg=Submission+received+with+ID+27295278

def test(base: int, nr_digits: int):
	mem = []
	counter = 1
	previous_result = 0
	result = 0
	while counter <= nr_digits:
		if counter > 1:
			result = (sum(mem) + 1) * (base-1) - (sum(mem[:len(mem)-1])+1)
		else:
			result = (previous_result + 1) * (base-1)

		previous_result = result
		mem.append(result)
		counter += 1
	return result


def main():

	inputs = []
	while True:
		base, nr_digits = map(int, input().split())
		if base == 0 and nr_digits == 0:
			break
		nr_digits = abs(nr_digits)
		inputs.append((base, nr_digits))

	for base, nr_digits in inputs:
		#print(count_superlucky_numbers(base, nr_digits))
		print(test(base, nr_digits))



if __name__ == "__main__":
	main()