# Dynamical programming
# Link to problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=649&page=show_problem&problem=1625


def convert_input_to_numbers(raw_input: str):

	result = []
	seq_len = int(raw_input[:raw_input.find(" ")])
	raw_input = raw_input[raw_input.find(" ")+1: ]

	while seq_len > 0:
		whitespace = raw_input.find(" ")
		if whitespace != -1:
			next_digit = raw_input[:whitespace]
			result.append(int(next_digit))
		else:
			result.append(int(raw_input))
			return result

		raw_input = raw_input[whitespace+1: ]
		seq_len -= 1


def find_best_sequence(numerical_input: list):
	idx = len(numerical_input) - 1
	max_reward = 0
	cumulative_reward = 0

	while idx >= 0:

		# update cumulative reward
		if cumulative_reward < 0 and numerical_input[idx] > 0:
			cumulative_reward = numerical_input[idx]
		else:
			cumulative_reward += numerical_input[idx]

		# update max reward
		if cumulative_reward > max_reward:
			max_reward = cumulative_reward

		idx -= 1

	return max_reward


def main():
	raw_input = input("input numbers")
	numerical_input = convert_input_to_numbers(raw_input)
	result = find_best_sequence(numerical_input)

	if result == 0:
		print("Losing streak.")
	else:
		print(f"The maximum winning streak is {result}")


if __name__ == "__main__":
	main()