# Programming challenges
# Link to problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=94&page=show_problem&problem=1012


def find_distance(case: tuple):
	speed, time = case
	result = (speed / 2) * time * 4
	if result < 0:
		return int(result-0.5)
	return int(result + 0.5)  # rounding


def main():
	inputs = []
	while True:
		try:
			a, b = map(int, input().split())
		except:
			break
		inputs.append((a, b))

	for case in inputs:
		print(find_distance(case))


if __name__ == "__main__":
	main()