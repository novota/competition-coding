# Link t problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=78&page=show_problem&problem=2692
buffer = {}
precomputed = {}

def precompute():
	global precomputed
	# number zero has zero adjecent bits
	precomputed[0] = 0
	pocet_bitu = 1
	precomputed[2**pocet_bitu - 1] = 0

	while pocet_bitu < 64:
		pocet_bitu += 1
		precomputed[2**pocet_bitu - 1] = 2 * precomputed[2**(pocet_bitu-1) - 1] + 2**(pocet_bitu-2)

	return precomputed


def compute_result(number: int):
	global precomputed
	if number == 0:
		return 0
	result = 0

	known_result = 2**(number.bit_length() - 1) - 1
	result += precomputed[known_result]
	remainder = number - known_result - 1

	if remainder == 0:
		return result

	# count neighbors caused by the leading bit which is one
	if remainder > known_result // 2:
		result += remainder - known_result // 2

	result += compute_result(remainder)
	return result


def main():
	precompute()
	input_nr = int(input())
	inputs = []

	while input_nr >= 0:
		inputs.append(input_nr)
		input_nr = int(input())

	for idx, number in enumerate(inputs):
		print("Case " + str(idx+1) + ": " + str(compute_result(number)))


if __name__ == "__main__":
	main()