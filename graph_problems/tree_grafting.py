# Dynamical programming challenges
# Link to problem
# https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=8&category=302&page=show_problem&problem=1822
counter = 0
inp = ""
max_depth = 0
depth1 = 0
max_depth1 = 0

class Node:
	def __init__(self, depth):
		global counter, max_depth
		self.value = counter
		self.left = None
		self.right = None
		self.depth = depth + 1
		if self.depth > max_depth:
			max_depth = self.depth
		counter += 1


def create_binary_tree(node):
	global counter, inp, depth1, max_depth1
	if not inp:
		return

	if inp[0] == "d":
		node.left = Node(node.depth)
		inp = inp[1:]
		depth1 += 1
		if depth1 > max_depth1:
			max_depth1 = depth1
		create_binary_tree(node.left)

	if inp[:2] == "ud":
		node.right = Node(node.depth)
		inp = inp[2:]
		create_binary_tree(node.right)
	else:
		depth1 -= 1
		inp = inp[1:]
		return

	return


def get_heights(curr: str):
	global inp, counter, max_depth, max_depth1, depth1
	inp = curr
	counter = 0
	max_depth = 0
	max_depth1 = 0
	depth1 = 0
	node = Node(-1)
	create_binary_tree(node)

	return

def main():
	global max_depth1, max_depth
	inputs = []
	curr = input()
	while curr != "#":
		inputs.append(curr)
		curr = input()

	for idx, curr in enumerate(inputs):
		get_heights(curr)
		print("Tree " + str(idx + 1) + ": " + str(max_depth1) + " => "  + str(max_depth))
	exit(0)

if __name__ == "__main__":
	main()
