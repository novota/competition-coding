import java.util.*;

public class Main {

    public static void main(String[] args) {

        int n;
        int x, y, z;
        ArrayList<Tuple[]> all_inputs = new ArrayList<>();

        // read input

        Scanner sc = new Scanner(System.in);

        while (true) {
            n = sc.nextInt();

            // stop condition
            if (n == 0) break;

            Tuple[] all_blocks = new Tuple[n];

            for (int i = 0; i < n; i++) {
                x = sc.nextInt();
                y = sc.nextInt();
                z = sc.nextInt();
                // add to list
                all_blocks[i] = new Tuple(x, y, z);
            }

            all_inputs.add(all_blocks);
        }

        // Compute results
        for (Tuple[] one_case : all_inputs) {
            solve_case(one_case);
        }
    }

    private static void solve_case(Tuple[] one_case) {
        // create all permutations
        List<Tuple> all_permutations = new ArrayList<Tuple>(one_case.length * 6);
        int all_permutations_index = 0;

        // go through block one by one and for each one create all permutations
        for (Tuple block : one_case) {
            Tuple[] permutations = block.create_permutations();
            // save permutations in all permutations
            for (Tuple permutation : permutations) {
                all_permutations.add(all_permutations_index, permutation);
                all_permutations_index++;
            }
        }

        // sort blocks so that the smallest one is at index 0
        all_permutations.sort(Comparator.comparingInt(o -> o.x));

        // find all connections and save max height
        int max_height = 0;
        for (int first_idx=0; first_idx < all_permutations.size(); first_idx++){
            Tuple block = all_permutations.get(first_idx);
            // in case there is only one element or multiple elements where neither can be placed on top of others
            if (block.z > max_height) max_height = block.z;
            // iterate over the rest of the list and find all connections possible
            for (int second_idx=first_idx+1; second_idx < all_permutations.size(); second_idx++){
                Tuple next_block = all_permutations.get(second_idx);

                // if both x and y of next block are bigger, then block can be placed on the next block
                if (next_block.y > block.y && next_block.x > block.x){
                    int tmp_height = block.max_height + next_block.z;
                    // save new height if its bigger then the current one
                    if (tmp_height > next_block.max_height) next_block.max_height = tmp_height;
                    // if tmp height is bigger than global max, update global bax
                    if (tmp_height > max_height) max_height = tmp_height;
                }
            }
        }
        System.out.println(max_height);
    }

}

class Tuple {
    /**
     Class that stores information about one block
     */
    public int x, y, z, max_height;

    public Tuple(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
        this.max_height = this.z;
    }

    public String toString()
    {
        return "x = " + x + ", y = " + y + ", z = " + z + " , max_height: " + max_height + ";";
    }

    /**
     * Create all permutations for one block
     */
    public Tuple[] create_permutations (){

        Tuple[] result = new Tuple[6];

        result[0] = new Tuple(this.x, this.y, this.z);
        result[1] = new Tuple(this.x, this.z, this.y);
        result[2] = new Tuple(this.y, this.x, this.z);
        result[3] = new Tuple(this.y, this.z, this.x);
        result[4] = new Tuple(this.z, this.y, this.x);
        result[5] = new Tuple(this.z, this.x, this.y);

        return result;
    }

}
