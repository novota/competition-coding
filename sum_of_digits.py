# Programming challenges
# Link to problem
# https://www.spoj.com/problems/CPCRC1C/
memory = {}

def precomputation():
	global memory
	# from one to ten
	start = 0
	summ = 0
	while start < 9:
		memory[start] = summ
		start += 1
		summ += start
	nine = "9"
	curr_nr = nine
	memory[int(nine)] = 45

	powers_of_ten = 10
	idx = 1
	while idx < 9:
		prev_nr = curr_nr
		curr_nr += nine
		memory[int(curr_nr)] = 10*memory[int(prev_nr)] + powers_of_ten*memory[9]
		idx += 1
		powers_of_ten *= 10


def sum_no_increment(a: str):
	if a in memory:
		return memory[a]

	result = int(a[0])

	if len(a) > 1:
		tmp = sum_no_increment(a[1:])
		memory[a] = tmp + result
		result += tmp
	return result


def find_digit_sum(a: str, a_int: int, b: str, b_int: int):
	global memory
	result = 0

	while a_int <= b_int:
		if len(a) > 1:
			result += int(a[0])
			the_rest = a[1:]
			if the_rest in memory:
				result += memory[the_rest]
			else:
				result += sum_no_increment(the_rest)

		else:
			result += a_int
		a_int += 1
		a = str(a_int)
	return result


def correct_result(a: int):
	if a <= 0:
		return 0
	if a in memory:
		return memory[a]
	result = 0
	# get number of digits
	a_len = len(str(a))
	known_result = ""
	for i in range(a_len - 1):
		known_result += "9"
	known_result = int(known_result) if len(known_result) > 0 else 0
	# one digit number is saved in memory
	if known_result == 0:
		return memory[a]
	result += memory[known_result]

	rest = a - known_result
	# leading digits
	result += 10**(a_len - 1) * memory[rest // 10**(a_len - 1)]
	#  remaining leading digits
	result += rest % (10**(a_len - 1)) * (a // 10**(a_len - 1))
	# known result repeats itself some number of times
	result += (rest // 10**(a_len - 1)) * memory[known_result]
	# find value of the rest
	result += correct_result(rest % (10**(a_len - 1)) - 1)

	memory[a] = result
	return result


def main():
	inputs = []
	precomputation()

	while True:
		a, b = map(int, input().split())

		if (a < 0) and (b < 0):
			break
		inputs.append((a, b))

	for a, b in inputs:
		#print(find_digit_sum(a, a_int, b, b_int))
		print(correct_result(b) - correct_result(a-1))



if __name__ == "__main__":
	main()
