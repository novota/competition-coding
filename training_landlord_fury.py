# Dyanmical programming
# problem link
# https://www.spoj.com/problems/NFURY/
import math
precomputed = []
buffer = {}


def get_result(value: int):
	global precomputed, buffer
	if value == 0:
		return 0

	if value in buffer:
		return buffer[value]

	best_result = 10000

	str_idx = precomputed.index(int(math.sqrt(value))**2)

	first_try = precomputed[str_idx]
	starting_idx = str_idx
	while str_idx >= starting_idx // 2:

		result = get_result(value - first_try)

		if result < best_result:
			best_result = result

		str_idx -= 1
		first_try = precomputed[str_idx]

	buffer[value] = best_result + 1

	return best_result + 1

def compute_squares():
	global precomputed
	nr = 1
	while nr**2 <= 1000:
		precomputed.append(nr**2)
		nr += 1



def main():
	# neglect first input
	nr_inputs = int(input())
	input_cases = []
	compute_squares()

	while nr_inputs > 0:
		input_cases.append(int(input()))
		nr_inputs -= 1

	for i in range(len(input_cases)):
		print(get_result(input_cases[i]))



if __name__ == "__main__":
	main()