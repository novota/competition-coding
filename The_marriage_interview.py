# Dynamical programming
# Link to problem
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=655&page=show_problem&problem=1387

buffer = {}

def decode_input(input_tuple:str):
	return int(input_tuple[: input_tuple.find(" ")]), int(input_tuple[input_tuple.find(" ") + 1: ])


def function_calls_counter(n, back):
	global buffer

	if n <= 1:
		return 0

	if n in buffer:
		return buffer[n]
	result = 0

	for i in range(1, back+1):
		result += function_calls_counter(n-i, back)

	result += back
	if n not in buffer:
		buffer[n] = result

	return result



def main():
	global buffer
	input_cases = []
	input_tuple = input("input")
	n, back = decode_input(input_tuple)

	while n < 61:
		input_cases.append((n, back))
		# next input
		input_tuple = input()
		n, back = decode_input(input_tuple)

	case_nr = 1
	for n, back in input_cases:
		# clear buffer
		buffer.clear()
		result = function_calls_counter(n, back) + 1
		print(f"Case {case_nr}: {result}")
		case_nr += 1


if __name__ == "__main__":
	main()