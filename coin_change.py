# Problem description
# https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=8&page=show_problem&problem=615
buffer = {}


def count_change_options(money, change, previous_coin=1000, coins_used=""):
	global buffer

	# recursion end
	if money == 0:
		return 1

	result = 0

	# if result already saved in buffer, use it
	if (previous_coin, money) in buffer:
		return buffer[(previous_coin, money)]

	for coin_value in change:
		if coin_value <= money and coin_value <= previous_coin:
			result += count_change_options(money - coin_value, change, coin_value)

	# save result in buffer for future use. we need to know, which coin was used last, because then we can use only coins lesser or equal to that
	if money not in buffer:
		buffer[(previous_coin, money)] = result

	return result



def main():
	change = [50, 25, 10, 5, 1]
	money = 7489
	result = count_change_options(money, change)
	print(result)



if __name__ == "__main__":
	main()


